import React, { useState } from "react";
import Home from "./Steps/Home";
import Catalog from "./Steps/Catalog";
import Calculator from "./Steps/Calculator";
import Progress from "../components/Progress";
import AppContext from "../AppContext";

const Main = () => {
  const [step, setStep] = useState(1);
  const [progress, setProgress] = useState(10);
  const [appState, setAppState] = useState({});

  const goToStep2 = ({ apartPrice, entry }) => {
    console.log(entry);
    console.log(apartPrice);
    setAppState({ ...appState, entry: entry, apartPrice: apartPrice });
    setProgress(50);
    setStep(2);
  };

  const goToStep3 = ({ montlyPayment, paymentYears }) => {
    console.log(montlyPayment);
    console.log(paymentYears);
    setAppState({
      ...appState,
      montlyPayment: montlyPayment,
      paymentYears: paymentYears
    });
    setProgress(80);
    setStep(3);
  };

  return (
    <AppContext.Provider value={appState}>
      <Progress progress={progress} />
      {step === 1 && <Home onClickContinue={values => goToStep2(values)} />}
      {step === 2 && (
        <Calculator onClickContinue={values => goToStep3(values)} />
      )}
      {step === 3 && <Catalog />}
    </AppContext.Provider>
  );
};

export default Main;
