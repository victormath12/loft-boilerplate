import styled from "styled-components";

export const CatalogStyled = styled.section`
  padding-top: 80px;
  padding-bottom: 64px;
`;
