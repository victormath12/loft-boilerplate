import React, { useState, useEffect } from "react";
import { Container, Grid } from "@material-ui/core";
import { H2 } from "../../../components/Typography";
import { CatalogStyled } from "./Catalog.style";
import CardCatalog from "../../../components/CardCatalog";

import axios from "axios";

export default function Catalog() {
  const [homes, setHomes] = useState([]);

  useEffect(() => {
    loftDataCrawler();
  }, []);

  const loftDataCrawler = async () => {
    let possibleAparts = await axios.get(
      `https://raw.githubusercontent.com/geovanipfranca/loft-easy-backend/master/public/data/loftDataFull.json`
    );
    const filteredAparts = possibleAparts.data.filter(
      apart => apart.probabilidade_venda > "Baixa"
    );
    const mappledAparts = filteredAparts.map(apart => {
      return {
        id: apart.id,
        price: apart.preco_venda,
        shortAddress: apart.endereco.bairro,
        completeAddress: `${apart.endereco.bairro}, ${apart.endereco.cidade}`,
        size: apart.area_total,
        dorms: apart.suites,
        thumb: apart.media
      };
    });
    setHomes(mappledAparts);
    return mappledAparts;
  };

  return (
    <>
      <CatalogStyled>
        <Container maxWidth="lg">
          <Grid container spacing={4}>
            <Grid item xs={12}>
              <H2 light>Olha só os apês que encontramos para você:</H2>
              <Grid container>
                {homes.map((result, index) => {
                  console.log(result);
                  return (
                    <Grid item xs="auto" style={{ margin: 10 }}>
                      <CardCatalog key={index} data={result} />
                    </Grid>
                  );
                })}
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </CatalogStyled>
    </>
  );
}
