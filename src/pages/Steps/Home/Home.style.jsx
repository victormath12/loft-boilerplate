import styled from "styled-components";
import H1, { H4 } from "../../../components/Typography";
import CurrencyInput from "react-currency-input";

import { family } from "../../../assets";

export const HeaderStyled = styled.header`
  height: 100vh;
  padding-top: 32px;

  background: #ff8008;
  background: -webkit-linear-gradient(to right, #ff8008, #ffc837);
  background: linear-gradient(to right, #ff8008, #ffc837) @media
    (min-width: 768px) {
    background-image: url(${family});
    background-size: auto 100%;
    background-position: right;
    background-repeat: no-repeat;
  }
`;

export const LoftLogo = styled.img`
  width: 100px;
  margin-top: 8px;
`;

export const HeaderTitle = styled(H1)`
  width: 257px;
  margin-top: 48px;
  color: #ffffff;

  @media (min-width: 768px) {
    margin-top: 72px;
    width: 599px;
  }
`;

export const HeaderSubtitle = styled(H4)`
  width: 223px;
  color: #ffffff;
  margin-top: 16px;

  @media (min-width: 768px) {
    margin-top: 24px;
    width: 467px;
  }
`;

export const Form = styled.form`
  display: flex;
  justify-content: space-between;
  flex-wrap: nowrap;
  width: 80%;
  background-color: #ffffff;
  padding: 20px;
  max-width: none;
  margin-top: 40px;
  margin-bottom: 64px;
  border-radius: 8px;

  @media (max-width: 768px) {
    flex-wrap: wrap;
    margin-top: 32px;
    margin-bottom: 24px;
  }
`;

export const FormAction = styled.section`
  width: 100%;

  @media (min-width: 768px) {
    width: 176px;
  }
`;

export const InputWrapper = styled.div`
  display: flex;
  flex: 1;
`;

export const InputStyled = styled(CurrencyInput)`
  padding: 20px;
  width: auto;
  flex: 1;
  font-size: 20px;
  border: none;
`;

export const LabelStyled = styled.label`
  position: absolute;
  font-size: 10px;
  padding-left: 20px;
`;
