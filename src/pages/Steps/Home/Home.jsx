import React, { useState } from "react";
import { Container, Button } from "@material-ui/core";

import {
  HeaderStyled,
  LoftLogo,
  HeaderTitle,
  HeaderSubtitle,
  Form,
  InputWrapper,
  InputStyled,
  LabelStyled
} from "./Home.style";

import { logo } from "../../../assets";

const Home = ({ onClickContinue, ...props }) => {
  const [entry, setEntry] = useState(undefined);
  const [apartPrice, setApartPrice] = useState(undefined);

  const handleEntry = (event, maskedvalue, floatvalue) => {
    setEntry(floatvalue);
  };

  const handleApartPrice = (event, maskedvalue, floatvalue) => {
    setApartPrice(floatvalue);
  };

  return (
    <HeaderStyled>
      <Container maxWidth="lg">
        <LoftLogo src={logo} alt="loft-logo" />
        <HeaderTitle>
          Já imaginou pagar a entrada do seu apartamento vivendo nele?
        </HeaderTitle>
        <HeaderSubtitle light>
          Com a loft easy você pode ter seu apartamento totalmente reformado com
          até zero de entrada.
        </HeaderSubtitle>
        <Form>
          <InputWrapper>
            <LabelStyled>Qual o valor do apê dos sonhos?</LabelStyled>
            <InputStyled
              value={apartPrice}
              prefix="R$"
              decimalSeparator=","
              thousandSeparator="."
              onChangeEvent={handleApartPrice}
            />
          </InputWrapper>
          <InputWrapper>
            <LabelStyled>
              Quanto você tem hoje para usar de entrada?
            </LabelStyled>
            <InputStyled
              value={entry}
              prefix="R$"
              placeholder="Quanto você tem hoje para usar de entrada?"
              decimalSeparator=","
              thousandSeparator="."
              onChangeEvent={handleEntry}
            />
          </InputWrapper>
          <Button
            variant="contained"
            color="secondary"
            disabled={!entry || !apartPrice}
            onClick={() => onClickContinue({ apartPrice, entry })}
            style={{ minWidth: 200 }}
          >
            Próximo
          </Button>
        </Form>
      </Container>
    </HeaderStyled>
  );
};

export default Home;
