import styled from "styled-components";
import { H4 } from "../../../components/Typography";

export const CalculatorStyled = styled.section`
  padding-top: 80px;
  padding-bottom: 64px;
`;

export const ResultWrapper = styled.div`
  margin: 20px;
`;

export const ResultContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const BigNumber = styled(H4)`
  font-size: 32px !important;
  color: #ff5722;
`;

export const ShortDescription = styled.p`
  margin: 0;
  padding: 0;
  color: #555;
`;
