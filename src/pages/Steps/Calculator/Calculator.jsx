import React, { useContext, useState, useEffect } from "react";
import { Container, Grid, Button, Card, CardContent } from "@material-ui/core";
import { H2, H4 } from "../../../components/Typography";
import { CalculatorStyled } from "./Calculator.style";
import SliderCalculator from "../../../components/SliderCalculator/Slider";

import {
  ResultContainer,
  ResultWrapper,
  BigNumber,
  ShortDescription
} from "./Calculator.style";

import AppContext from "../../../AppContext";

const Calculator = ({ onClickContinue }) => {
  const appContext = useContext(AppContext);

  const [paymentYears, setPaymentYears] = useState(1);
  const [montlyPayment, setMontlyPayment] = useState(1);

  const entryNeeded = appContext.apartPrice * (20 / 100);
  const entryRest = entryNeeded - appContext.entry;

  useEffect(() => {
    calculateMontlyPayment(paymentYears);
  });

  const calculateMontlyPayment = async years => {
    await setPaymentYears(years);
    await setMontlyPayment(Math.round(entryRest / (years * 12)));
  };

  return (
    <>
      <CalculatorStyled>
        <Container maxWidth="lg">
          <Grid container spacing={4}>
            <Grid item xs={12}>
              <H2 light>
                Defina como você acha melhor pagar sua entrada já morando no
                imóvel:
              </H2>
            </Grid>
            <Grid item xs={12} md={6}>
              <SliderCalculator
                defaultValue={paymentYears}
                onChangeCommitted={(event, value) =>
                  calculateMontlyPayment(value)
                }
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <Card>
                <CardContent>
                  <ResultContainer>
                    <ResultWrapper>
                      <H4>R${appContext.apartPrice}</H4>
                      <ShortDescription>preço do Apê</ShortDescription>
                    </ResultWrapper>
                    <ResultWrapper>
                      <H4>R${entryNeeded}</H4>
                      <ShortDescription>
                        valor da entrada (20% do total)
                      </ShortDescription>
                    </ResultWrapper>
                  </ResultContainer>

                  <ResultContainer>
                    <ResultWrapper>
                      <H4>R${appContext.entry}</H4>
                      <ShortDescription>
                        seu investimento para a entrada
                      </ShortDescription>
                    </ResultWrapper>
                    <ResultWrapper>
                      <H4>R${entryRest}</H4>
                      <ShortDescription>
                        saldo restante para a entrada
                      </ShortDescription>
                    </ResultWrapper>
                  </ResultContainer>
                  <ResultContainer>
                    <ResultWrapper>
                      <BigNumber>R${montlyPayment}</BigNumber>
                      <ShortDescription>
                        durante {paymentYears} anos irá pagar a entrada
                      </ShortDescription>
                    </ResultWrapper>
                  </ResultContainer>
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs={12}>
              <Button
                variant="contained"
                color="primary"
                onClick={() => onClickContinue({ montlyPayment, paymentYears })}
              >
                Próximo
              </Button>
            </Grid>
          </Grid>
        </Container>
      </CalculatorStyled>
    </>
  );
};

export default Calculator;
