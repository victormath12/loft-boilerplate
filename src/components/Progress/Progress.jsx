import React from "react";
import { lighten, makeStyles, withStyles } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";

const BorderLinearProgress = withStyles({
  root: {
    height: 10,
    backgroundColor: lighten("#fff", 0.5)
  },
  bar: {
    borderRadius: 20,
    backgroundColor: "#e25018"
  }
})(LinearProgress);

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  }
}));

const Progress = ({ progress }) => {
  const classes = useStyles();
  return (
    <BorderLinearProgress
      className={classes.margin}
      variant="determinate"
      color="secondary"
      value={progress}
    />
  );
};

export default Progress;
