import React from "react";
import {
  Card,
  CardContent,
  CardActions,
  CardMedia,
  Button,
  Typography
} from "@material-ui/core";

import { home } from "../../assets";

const CardCatalog = ({ data }) => (
  <Card style={{ maxWidth: 345 }}>
    <CardMedia
      style={{ height: 140 }}
      image={data.thumb}
      title={data.shortAddress}
    />
    <CardContent>
      <Typography gutterBottom variant="h5" component="h2">
        {data.shortAddress}
      </Typography>
      <Typography variant="body2" color="textSecondary" component="p">
        {data.completeAddress}
      </Typography>
    </CardContent>
    <CardActions></CardActions>
  </Card>
);

export default CardCatalog;
