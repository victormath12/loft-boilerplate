import styled from 'styled-components'
import { TextField as MuiTextField } from '@material-ui/core'

export const TextField = styled(MuiTextField)`
  width: 100%;

  & .MuiFormHelperText-root {
    color: #F46152;
  }
`
