import React from "react";
import styled from "styled-components";
import { TextField } from "./Input.style";

export default function Input({
  type,
  label,
  name,
  reference,
  errorMessage,
  defaultValue,
  ...props
}) {
  return (
    <TextField
      type={type}
      label={label}
      name={name}
      inputRef={reference}
      helperText={errorMessage}
      variant="standard"
      defaultValue={defaultValue}
      style={{ marginRigth: 10 }}
      {...props}
    />
  );
}
