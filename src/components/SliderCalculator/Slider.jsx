import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Slider from "@material-ui/core/Slider";
import Typography from "@material-ui/core/Typography";
import { H4, H5 } from "../../components/Typography";

const useStyles = makeStyles(theme => ({
  root: {
    width: "80%"
  },
  margin: {
    height: theme.spacing(3)
  }
}));

function valuetext(value) {
  return `${value} anos`;
}

const SliderCalculator = ({ ...props }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography id="discrete-slider" gutterBottom>
        Quantos anos como Loft easy:
      </Typography>
      <Slider
        getAriaValueText={valuetext}
        aria-labelledby="discrete-slider"
        valueLabelDisplay="auto"
        step={1}
        marks
        min={1}
        max={4}
        {...props}
      />
    </div>
  );
};

export default SliderCalculator;
