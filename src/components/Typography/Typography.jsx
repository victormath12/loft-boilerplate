import styled from "styled-components";

export default styled.h1`
  font-family: Lato;
  font-style: normal;
  font-weight: 800;
  font-size: 28px;
  line-height: 34px;

  @media (min-width: 768px) {
    font-size: 48px;
    line-height: 58px;
  }
`;

export const H2 = styled.h2(
  ({ light }) => `
  font-family: Lato;
  font-style: normal;
  font-weight: ${light ? "300" : "800"};
  font-size: 40px;
  line-height: 48px;
  margin: 0;
`
);

export const H4 = styled.h4(
  ({ light }) => `
  font-family: Lato;
  font-style: normal;
  font-weight: ${light ? "300" : "800"};
  font-size: 18px;
  line-height: 44px;
  margin: 0;

  @media(min-width: 768px) {
    font-size: 24px;
    line-height: 30px;
  }
`
);

export const H5 = styled.h5`
  font-family: Lato;
  font-style: normal;
  font-weight: 800;
  font-size: 20px;
  line-height: 24px;
  margin: 0;
`;

export const H6 = styled.h6`
  font-family: Lato;
  font-style: normal;
  font-weight: 800;
  font-size: 18px;
  line-height: 22px;
  margin: 0;
`;

export const Body1 = styled.p`
  font-family: Lato;
  font-style: normal;
  font-weight: 300;
  font-size: 16px;
  line-height: 24px;
  margin: 0;
`;

export const Body2 = styled.p`
  font-family: Lato;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 24px;
  margin: 0;
`;
