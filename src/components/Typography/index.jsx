import H1, { H2, H4, H5, H6, Body1, Body2 } from "./Typography";

export { H1 as default, H2, H4, H5, H6, Body1, Body2 };
