import React from "react";
import AppRouter from "./Router";
import { createGlobalStyle, ThemeProvider } from "styled-components";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./theme";

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    color: ${props => props.theme.fontColor};
    font-family: ${props => props.theme.fontFamily};
    background-color: #f5f5fa;
  }
`;

function App() {
  return (
    <ThemeProvider theme={theme}>
      <MuiThemeProvider theme={theme}>
        <GlobalStyle />
        <AppRouter />
      </MuiThemeProvider>
    </ThemeProvider>
  );
}

export default App;
