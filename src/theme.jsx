import { red } from "@material-ui/core/colors";
import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  fontFamily: "Lato",
  fontColor: "#191f23",
  palette: {
    primary: {
      main: "#ff5722"
    },
    secondary: {
      main: "#e25018"
    },
    error: {
      main: red.A400
    },
    background: {
      default: "#fff"
    }
  }
});

export default theme;
