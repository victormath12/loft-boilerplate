import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Main from "./pages";

const appPages = [{ name: "home", path: "/", component: Main }];

const AppRouter = () => (
  <BrowserRouter>
    <Switch>
      {appPages.map(value => (
        <Route
          exact
          key={value.name}
          path={value.path}
          component={value.component}
        />
      ))}
    </Switch>
  </BrowserRouter>
);

export default AppRouter;
